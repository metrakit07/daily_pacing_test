<?php

namespace App\Console\Commands;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DailyPacing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily-pacing:calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command that calculate and store the daily_app_limit for each clients';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        // Request to get the number of deliveries per clients
        $orders = Order::selectRaw( "`order`.*, COUNT(*) as nb_deliveries" )
            ->join( 'delivery AS del', 'del.client_id', 'order.client_id' )
            ->whereBetweenColumns( 'del.delivery_date', [ 'order.period_from', 'order.period_to' ] )
            ->groupByRaw( 'id, client_id, period_from, period_to, period_app_limit, daily_app_limit' )->get();

        foreach ( $orders as $odr ){
            $odr->daily_app_limit = $this->findDailyPacing( $odr );
            $odr->save();
        }
        return 0;
    }

    /**
     * Return the daily pacing for a client
     *
     * @param   Order $odr
     *
     * @return int|null
     */
    private function findDailyPacing( Order $odr ): ?int
    {
        $periodFrom     = Carbon::parse( $odr->period_from      );
        $periodTo       = Carbon::parse( $odr->period_to        );
        $durationPeriod = $periodTo->diffInDays( $periodFrom    );
        $dayLeft        = now()->diffInDays( $periodTo          );

        // if we over delivered or if we already reached the limit
        if( ( $nb_deliveries_left = $odr->period_app_limit - $odr->nb_deliveries ) <= 0 ){
            return 0;
        }

        // if the period left is equal or under the 'no pacing pourcentage' no pacing apply
        if( ( $dayLeft  / $durationPeriod ) * 100 <= Order::POURCENTAGE_NO_PACING ){
            return null;
        }

        $timeSincePeriodStart   = now()->diffInSeconds( $periodFrom );
        $timePeriodFull         = $periodTo->diffInSeconds( $periodFrom );

        // if underpacing no pacing apply
        if( ( $odr->nb_deliveries / $odr->period_app_limit ) < ( $timeSincePeriodStart / $timePeriodFull ) ){
            return null;
        }

        return (int)round( $nb_deliveries_left / $dayLeft );
    }
}
