<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $table = 'order';
    public $timestamps = false;
    public $guarded = [];

    /**
     * No Pacing pourcentage
     *
     * @var int
     */
    public CONST POURCENTAGE_NO_PACING = 15;
}
